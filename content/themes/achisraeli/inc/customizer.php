<?php
/**
 * achisraeli Theme Customizer
 *
 * @package achisraeli
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function achisraeli_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	$allPosts = get_posts( array(
					'numberposts' => -1,
				    'post_type' => array('page','post'),
				    'orderby'   => 'post__in',
				));
	foreach ( $allPosts as $post ) {
		$posts[$post->ID] = $post->post_title;
        
    }

    // Add page dropdown

	$wp_customize->add_setting( 'tc' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'tc', array(
	    'label' => __( '"T&C " page', 'achisraeli' ),
	    'section' => 'title_tagline',
	    'settings' => 'tc',
	    'type'    => 'select',
    		'choices' => $posts
	) ) );

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'achisraeli_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'achisraeli_customize_partial_blogdescription',
		) );
	}
	// Add Copyright 
	$wp_customize->add_setting( 'copyright' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'copyright', array(
	    'label' => __( 'copyright', '_jpl' ),
	    'section' => 'title_tagline',
	    'settings' => 'copyright',
	    'priority' => 30,
	) ) );
	// Add Social Media Section
	$wp_customize->add_section( 'social-media' , array(
	    'title' => __( 'Social Media', 'achisraeli' ),
	    'priority' => 30,
	    'description' => __( 'Enter the URL to your account for each service for the icon to appear in the footer.', 'achisraeli' )
	) );
	// Add Youtube Setting
	$wp_customize->add_setting( 'youtube' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'youtube', array(
	    'label' => __( 'Youtube', 'achisraeli' ),
	    'section' => 'social-media',
	    'settings' => 'youtube',
	) ) );
	// Add Twitter Setting
	$wp_customize->add_setting( 'twitter' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'twitter', array(
	    'label' => __( 'Twitter', 'achisraeli' ),
	    'section' => 'social-media',
	    'settings' => 'twitter',
	) ) );
	// Add Facebook Setting
	$wp_customize->add_setting( 'facebook' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'facebook', array(
	    'label' => __( 'Facebook', 'achisraeli' ),
	    'section' => 'social-media',
	    'settings' => 'facebook',
	) ) );
	// Add Linkedin Setting
	$wp_customize->add_setting( 'linkedin' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'linkedin', array(
	    'label' => __( 'Linkedin', 'achisraeli' ),
	    'section' => 'social-media',
	    'settings' => 'linkedin',
	) ) );
	// Add instagram Setting
	$wp_customize->add_setting( 'instagram' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'instagram', array(
	    'label' => __( 'Instagram', 'achisraeli' ),
	    'section' => 'social-media',
	    'settings' => 'instagram',
	) ) );

	// Add Custom Code
	$wp_customize->add_section( 'additionalCode' , array(
	    'title' => __( 'Additional Code', 'achisraeli' ),
	    'priority' => 30,
	    'description' => __( 'Add code to the header and footer', 'achisraeli' )
	) );
	$wp_customize->add_setting( 'headerCode' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'headerCode', array(
	    'label' => __( '"Header Code', 'achisraeli' ),
	    'section' => 'additionalCode',
	    'settings' => 'headerCode',
	    'type'    => 'textarea',
	) ) );
	$wp_customize->add_setting( 'footerCode' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'footerCode', array(
	    'label' => __( '"Footer Code', 'achisraeli' ),
	    'section' => 'additionalCode',
	    'settings' => 'footerCode',
	    'type'    => 'textarea',
	) ) );
	$wp_customize->add_setting( 'apiKey' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'comeet_groups', array(
	    'label' => __( 'Google Map API Key', 'achisraeli' ),
	    'section' => 'additionalCode',
	    'settings' => 'apiKey',
	) ) );
}
add_action( 'customize_register', 'achisraeli_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function achisraeli_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function achisraeli_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function achisraeli_customize_preview_js() {
	wp_enqueue_script( 'achisraeli-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'achisraeli_customize_preview_js' );
