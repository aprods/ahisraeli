<?php
//Override Logo
// $meta_boxes[] = array(
//     'id' => 'overrideLogo',
//     'title' => __('Logo', "achisraeli"),
//     'pages' => array('page'),
//     'context' => 'side',
//     'priority' => 'high',
//     'fields' => array(
//         array(
//             'name' => __('', "achisraeli"),
//             'id' => 'Logo',
//             'type' => 'file',
//         )
//     )
// );

$meta_boxes[] = array(
    'id' => 'uploadPdf',
    'title' => __('Upload a PDF ', "achisraeli"),
    'pages' => array('pressreleases'),
    'context' => 'side',
    'priority' => 'low',
    'fields' => array(
        array(
            'name' => __('PDF', "achisraeli"),
            'id' => 'pPdf',
            'type' => 'file',
        )
    )
);


global $pagenow, $post;

if('post.php' == $pagenow){
    $postID= $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];

    $pageTemplate = get_post_meta($postID, '_wp_page_template', true);

    if($pageTemplate == 'page-home.php' ){
        

    }
    
    if($postID == get_option('page_for_posts') ){
        
    }
    $errorpageid = get_option( '404pageid', 0 );
    if($postID == $errorpageid ){
        $meta_boxes[] = array(
            'id' => 'mSelect',
            'title' => __('Link items', "achisraeli"),
            'pages' => array('page'),
            'context' => 'normal',
            'priority' => 'default',
            'fields' => array(
                array(
                    'name' => __('#1 label', "achisraeli"),
                    'id' => 'errlabel1',
                    'type' => 'text',
                ),
                array(
                    'name' => __('#1 link to page', "achisraeli"),
                    'id' => 'errlink1',
                    'type' => 'selectpages',
                ),
                array(
                    'name' => __('#2 label', "achisraeli"),
                    'id' => 'errlabel2',
                    'type' => 'text',
                ),
                array(
                    'name' => __('#2 link to page', "achisraeli"),
                    'id' => 'errlink2',
                    'type' => 'selectpages',
                ),
                array(
                    'name' => __('#3 label', "achisraeli"),
                    'id' => 'errlabel3',
                    'type' => 'text',
                ),
                array(
                    'name' => __('#3 link to page', "achisraeli"),
                    'id' => 'errlink3',
                    'type' => 'selectpages',
                ),
                array(
                    'name' => __('#4 label', "achisraeli"),
                    'id' => 'errlabel4',
                    'type' => 'text',
                ),
                array(
                    'name' => __('#4 link to page', "achisraeli"),
                    'id' => 'errlink4',
                    'type' => 'selectpages',
                )
                ,
                array(
                    'name' => __('#5 label', "achisraeli"),
                    'id' => 'errlabel5',
                    'type' => 'text',
                ),
                array(
                    'name' => __('#5 link to page', "achisraeli"),
                    'id' => 'errlink5',
                    'type' => 'selectpages',
                )
            )
        );
    }



}


foreach ($meta_boxes as $meta_box) {
    $my_box = new My_meta_box($meta_box);
}
class My_meta_box {

    protected $_meta_box;

    // create meta box based on given data
    function __construct($meta_box) {
        $this->_meta_box = $meta_box;
        add_action('admin_menu', array(&$this, 'add'));

        add_action('save_post', array(&$this, 'save'));
    }

    /// Add meta box for multiple post types
    function add() {
        foreach ($this->_meta_box['pages'] as $page) {
            add_meta_box($this->_meta_box['id'], __($this->_meta_box['title'],'achisraeli'), array(&$this, 'show'), $page, $this->_meta_box['context'], $this->_meta_box['priority']);
        }
    }

    // Callback function to show fields in meta box
    function show() {
        global $post;

        if (isset($this->_meta_box['description'])) {
            echo $this->_meta_box['description'];
        }


        // Use nonce for verification
        echo '<input type="hidden" name="mytheme_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

        echo '<div>';

        foreach ($this->_meta_box['fields'] as $field) {
            // get current post meta data
            $meta = get_post_meta($post->ID, $field['id'], true);

            $field['std']='';

            if (isset($field['className'])) {
                $className = $field['className'];
            }else{
                $className = 'clearfix floated';
            }


            echo '<div id="',$field['id'],'_wrapper">';
            if (isset($field['hr'])) {
                echo '<hr/>';
            }
            echo '<div class="'.$className.'"><label class="post-attributes-label" for="', $field['id'], '">', __($field['name'],'achisraeli'), '</label>';

            switch ($field['type']) {
	            case 'text':
		            echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30"  />';
	              if ( ! empty( $field['desc'] ) ) {
		              echo '<p style="margin-left: 20%;">', $field['desc'], '</p>';
	              }

                    break;
                case 'richtext':
                    $metaContent = $meta ? $meta : $field['std'];
                    wp_editor($metaContent, $field['id'], array(
                        'wpautop'       =>      true,
                        'media_buttons' =>      false,
                        'textarea_name' =>      $field['id'],
                        'textarea_rows' =>      10,
                        'teeny'         =>      true
                        ));
                    break;
                case 'btn':
                    echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" class="js-url" />';
                    echo '<button type="text" data-url="',get_template_directory_uri(),'" class="button button-primary button-small js-curl"  />Fetch</button>';

                    break;
                case 'timezone':

                    $tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

                        ?>

                                <select name="office_clock_timezone" id="office_timezone">
                                    <option value="">Choose a time zone</option>
                                <?php foreach($tzlist as $country){?>
                                <option <?php if(get_option('dt_clock'.$i.'_timezone')==$country){ ?>selected<?php } ?> value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                <?php } ?>
                                </select>
                                <script type="text/javascript">
                                jQuery(document).ready(function( $ ) {
                                    $('#office_timezone').on('change', function(){
                                        $("#<?php echo $field['id']?>").val($(this).val());
                                    });
                                    var office_timezone = $("#<?php echo $field['id']?>").val();
                                    if (office_timezone.length > 0){
                                        $("#office_timezone").val(office_timezone);
                                    }


                                });
                                </script>

                        <?php

                    echo '<input type="hidden" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" class="js-url" />';

                    break;
                case 'selectpages':

                        ?>
                        <div class="<?php echo $field['id'] ;?>">
                            <?php
                                wp_dropdown_pages();
                                echo '<input type="hidden" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" class="js-url" />';
                            ?>

                        </div>
                        <script type="text/javascript">
                            jQuery(document).ready(function( $ ) {
                                var fieldContainer = $(".<?php echo $field['id'];?> select");
                                fieldContainer.on('change', function(){
                                    $("#<?php echo $field['id']?>").val($(this).val());
                                });
                                var selectpages = $("#<?php echo $field['id']?>").val();
                                if (selectpages.length > 0){
                                    fieldContainer.val(selectpages);
                                }
                            });
                        </script>


                        <?php


                    break;
                case 'file':
                    wp_enqueue_media();
                    // Get WordPress' media upload URL
                    $upload_link = esc_url( get_upload_iframe_src( 'image', $post->ID ) );

                    // See if there's a media id already saved as post meta
                    $your_img_id = get_post_meta( $post->ID, $field['id'], true );

                    $your_img_url = wp_get_attachment_url( $your_img_id,true );

                    // Get the image src
                    $your_img_src = wp_get_attachment_image_src( $your_img_id, 'full' );
                    //$your_img_src =get_post_meta( $post->ID, $field['id'], true );

                    // For convenience, see if the array is valid
                    $you_have_img = is_array( $your_img_src ) ;
                    // $attachment_id = attachment_url_to_postid( $your_img_src );
                    // echo "<pre>";
                    // print_r('$upload_link: '.$upload_link.'<br>$your_img_id: '.$your_img_id.'<br>$your_img_url : '.$your_img_url.'<br>$you_have_img : '.$you_have_img );
                    // echo "</pre>";



                    echo '<!-- Your image container, which can be manipulated with js -->
                    <div id='.$field['id'].'_container" class="custom-img-container">';
                        if(!empty($your_img_id)){
                            if ( $you_have_img ) {
                                echo'<img src="'. $your_img_src[0] .'" alt="" style="max-width:100%;max-height:200px" />';
                            }else{
                                echo'<img src="/stg/wp-includes/images/media/document.png" alt="" /><br>';

                                // echo "<pre>";
                                // print_r('nonImgFile: '.$your_img_url );
                                // echo "</pre>";
                                if(!empty($your_img_url)){
                                    $nonImgFile = pathinfo($your_img_url);
                                    function filePathParts($arg1) {
                                        // echo $arg1['dirname'], "\n";
                                        echo '<em>',$arg1['basename'], "</em>";
                                        // echo $arg1['extension'], "\n";
                                        // echo $arg1['filename'], "\n";
                                    }
                                    filePathParts($nonImgFile);
                                }
                            }
                        }

                    echo '</div>';

                    echo '<!-- Your add & remove image links -->
                    <p class="hide-if-no-js">
                        <a id="'.$field['id'].'_upload"class="upload-custom-img ';
                         if ( !empty($your_img_id) ) :
                         echo'hidden';
                         endif;
                           echo'" href="'.$upload_link .'">';
                           _e('Upload file','achisraeli') ;
                        echo'</a>
                        <a id="'.$field['id'].'_del" class="delete-custom-img ';
                         if ( empty($your_img_id) ) :
                         echo'hidden';
                         endif;
                           echo'"href="#">';
                           _e('Remove','achisraeli') ;
                        echo'</a>
                    </p>

                    <!-- A hidden input to set and post the chosen image id -->
                    <input class="custom-img-id" name="', $field['id'], '" id="', $field['id'], '" type="hidden" value="',esc_attr( $your_img_id ),'" />';
                    echo "<script>
                    jQuery(function($){

                      // Set all variables to be used in scope
                      var frame,
                          metaBox = $('#".$field['id']."_wrapper'), // Your meta box id here
                          addImgLink = metaBox.find('.upload-custom-img'),
                          delImgLink = metaBox.find( '.delete-custom-img'),
                          imgContainer = metaBox.find( '.custom-img-container'),
                          imgIdInput = metaBox.find( '.custom-img-id' );
                      
                      // ADD IMAGE LINK
                      addImgLink.on( 'click', function( event ){
                        
                        event.preventDefault();
                        
                        // If the media frame already exists, reopen it.
                        if ( frame ) {
                          frame.open();
                          return;
                        }
                        
                        // Create a new media frame
                        frame = wp.media({
                          title: 'Select or Upload Media Of Your Chosen Persuasion',
                          button: {
                            text: 'Use this media'
                          },
                          multiple: false  // Set to true to allow multiple files to be selected
                        });

                        
                        // When an image is selected in the media frame...
                        frame.on( 'select', function() {
                          
                          // Get media attachment details from the frame state
                          var attachment = frame.state().get('selection').first().toJSON();

                            var fileType = attachment.mime;
                            var validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
                            if ($.inArray(fileType, validImageTypes) < 0) {
                                 // invalid file type code goes here.
                                imgContainer.append( '<img src=\"/stg/wp-includes/images/media/document.png\" alt=\"\" style=\"max-width:100%;max-height:200px\"/><br><em>'+attachment.filename+'</em>' );

                            }else{
                                // Send the attachment URL to our custom image input field.
                                  imgContainer.append( '<img src=\"'+attachment.url+'\" alt=\"\" style=\"max-width:100%;max-height:200px\"/>' );
                            }





                          

                          // Send the attachment id to our hidden input
                          imgIdInput.val( attachment.id );

                          // Hide the add image link
                          addImgLink.addClass( 'hidden' );

                          // Unhide the remove image link
                          delImgLink.removeClass( 'hidden' );
                        });

                        // Finally, open the modal on click
                        frame.open();
                      });
                      
                      
                      // DELETE IMAGE LINK
                      delImgLink.on( 'click', function( event ){

                        event.preventDefault();

                        // Clear out the preview image
                        imgContainer.html( '' );

                        // Un-hide the add image link
                        addImgLink.removeClass( 'hidden' );

                        // Hide the delete image link
                        delImgLink.addClass( 'hidden' );

                        // Delete the image id from the hidden input
                        imgIdInput.val( '' );

                      });

                    });
                    </script>";

                    break;
                case 'textarea':
                    echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" >', $meta ? $meta : $field['std'], '</textarea>';
                    break;
                case 'select':
                    echo '<select name="', $field['id'], '" id="', $field['id'], '">';
                    foreach ($field['options'] as $option) {
                        echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
                    }
                    echo '</select>';
                    break;
                case 'radio':
                    foreach ($field['options'] as $option) {
                        echo '<input type="radio" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
                    }
                    break;
                case 'checkbox':
                    echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
                    break;
                case 'color':
                    wp_enqueue_style( 'wp-color-picker' );
                    wp_enqueue_script( 'wp-color-picker');

                    echo '<input name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" type="text" class="', $field['id'],'-colorpicker" />';

                    echo "<script>
                        jQuery(function($) {
                            // turn the field into a colorpicker
                            $('.".$field['id']."-colorpicker').wpColorPicker () ;

                        });
                    </script>";
                    break;
                case 'date':
                    wp_enqueue_script( 'jquery-ui-datepicker' );
                    wp_enqueue_style( 'jquery-ui-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/smoothness/jquery-ui.css', true);


                    echo '<input name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" type="text" class="', $field['id'],'-datepicker" />';

                    echo "<script>
                        jQuery( document ).ready( function( $ ) {
                            $('#".$field['id']."').datepicker({
                                // dateFormat: 'mm.dd.yy',
                                defaultDate: '". $meta ."'
                            });
                        } );
                        </script>";
                    break;
                case 'time':
                    wp_enqueue_script( 'jquery-timepicker','https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js',true );
                    wp_enqueue_style( 'jquery-timepicker-style', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css', true);


                    echo '<input name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" type="text" class="', $field['id'],'-timepicker" />';

                    echo "<script>
                        jQuery( document ).ready( function( $ ) {
                            $('#".$field['id']."').timepicker({
                                timeFormat:'hh:mm TT'
                            });
                        } );
                        </script>";
                    break;
            }
            if (isset($field['description'])) {
                echo '<small><em>'.$field['description'].'</em></small>';
            }
            echo     '</div>';
            echo'</div>';


        }

        echo '</div>';
    }

    // Save data from meta box
    function save($post_id) {
        $post = get_post_type( $post_id);

        if($post != $this->_meta_box['pages'][0]) {
           return $post_id;
        }
        if ( isset($_POST['_wpnonce'])) {
            // verify nonce
            if (!wp_verify_nonce($_POST['mytheme_meta_box_nonce'], basename(__FILE__))) {

                return $post_id;
            }

            // check autosave
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
                return $post_id;
            }

            // check permissions
            if ('page' == $_POST['post_type']) {
                if (!current_user_can('edit_page', $post_id)) {
                    return $post_id;
                }
            } elseif (!current_user_can('edit_post', $post_id)) {
                return $post_id;
            }

            foreach ($this->_meta_box['fields'] as $field) {
                $old = get_post_meta($post_id, $field['id'], true);
                $new = $_POST[$field['id']];

                if ($new && $new != $old) {
                    update_post_meta($post_id, $field['id'], $new);
                } elseif ('' == $new && $old) {
                    delete_post_meta($post_id, $field['id'], $old);
                }
            }
        }
    }
}

function wpse_157410_custom_meta_box( $display_fields = array(), $cat , $ID = null, $echo = true ) {

    // if provided $ID is null fetch the current post
    $ID = is_null( $ID ) ? get_the_ID() : $ID;
    $cat = $cat;
    $output = '';
    $row_template = apply_filters( 'wpse_157410_custom_meta_box/row_template', '<tr><th scope="row">%s</th><td>%s</td></tr>' );
    $values_delimiter = apply_filters( 'wpse_157410_custom_meta_box/values_delimiter', ',' );
    $custom_post_meta = get_post_custom( $ID );

    foreach( $custom_post_meta as $key => $values ){

        $title = '';
        if( array_key_exists( $key, $display_fields ) || empty( $display_fields ) ){
            $title = empty( $display_fields ) ? $key : $display_fields[ $key ];
        }

        // skip to the next record if we can't display a title
        if( empty( $title ) )
            continue;

        $value = implode( $values_delimiter, $values );
        $output .= apply_filters( 'wpse_157410_custom_meta_box/row_template',
            sprintf( $row_template, $title, $value ),
            $values,
            $key,
            $ID,
            $custom_post_meta,
            $display_fields
            );
    }
    // $output .= '</table>';

    $output = apply_filters( 'wpse_157410_custom_meta_box/output', $output, $custom_post_meta, $ID, $display_fields, $echo );

    if( $echo ) {
        if (!empty($output)) {
            echo '<tr><td colspan="2" class="cat-sheet"><h4>' .$cat .' </h4> </td></tr>'. $output ;
        }
    } else {
        return $output;
    }
}

function achisraeli_after_title() {

    # Get the globals:
    global $post, $wp_meta_boxes;

    # Output the "afterTitle" meta boxes:
    do_meta_boxes(get_current_screen(), 'afterTitle', $post);

    # Remove the initial "afterTitle" meta boxes:
    unset($wp_meta_boxes['post']['afterTitle']);

}

add_action('edit_form_after_title', 'achisraeli_after_title');

