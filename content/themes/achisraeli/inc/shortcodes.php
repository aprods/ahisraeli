<?php
// Social shortcode
function social_func( $atts ) {
	$linkedin ='';
	$twitter ='';
	$facebook ='';
	$instagram ='';
	$youtube ='';

	if ( get_theme_mod('linkedin') ) {
		$linkedin = '<a class="linkedin-icon" href="'.get_theme_mod('linkedin') .'" target="_blank"><i class="fab fa-linkedin-in"></i></a>';
	}
	if ( get_theme_mod('twitter') ) {
		$twitter = '<a class="twitter-icon" href="'.get_theme_mod('twitter') .'" target="_blank"><i class="fab fa-twitter"></i></a>';
	}
	if ( get_theme_mod('facebook') ){
		$facebook = '<a class="facebook-icon" href="'.  get_theme_mod('facebook') .'" target="_blank"><i class="fab fa-facebook-f"></i></a>';
	}
	if ( get_theme_mod('instagram') ){
		$instagram = '<a class="instagram-icon" href="'.  get_theme_mod('instagram') .'" target="_blank"><i class="fab fa-instagram"></i></a>';
	}
	if ( get_theme_mod('youtube') ) {
		$youtube = '<a class="youtube-icon" href="'.get_theme_mod('youtube') .'" target="_blank"><i class="fab fa-youtube"></i></a>';
	}

	$social='<div class="social">'. $linkedin .$twitter.  $facebook . $instagram . $youtube ;
	$social.='<script type="application/ld+json">
				{
					"@context": "http://schema.org/",
					"@type": "Organization",
						"url":"http://webpals.com",
				        "sameAs": [
				    "'.  get_theme_mod('facebook') .'",
				    "'.get_theme_mod('twitter') .'",
				    "'.  get_theme_mod('instagram') .'",
				    "'.get_theme_mod('linkedin') .'",
				    "'.get_theme_mod('youtube') .'"
				  ]
				}
				</script>';
	$social.='</div>';
				
	return $social;
}
add_shortcode( 'social', 'social_func' );
