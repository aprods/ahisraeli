<?php
global $pagenow, $post;

if('post.php' == $pagenow){
    $postID= $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];

    $pageTemplate = get_post_meta($postID, '_wp_page_template', true);

    if($pageTemplate == 'page-mobile.php' || $pageTemplate == 'page-finance.php' ){
        // Add custom meta box for Image slider
        // if($pageTemplate == 'page-mobile.php' ){
        // // Add custom meta box for Image slider
        //     $title = "leading mobile logos";
        // }elseif($pageTemplate == 'page-finance.php' ){
        //     $title = "header Logos";
        // }
        add_action('admin_init', 'add_image_slider_meta_box',10,2);
        // add_filter( 'add_image_slider_meta_box', $title, 10, 2 );
    }


}    


function add_image_slider_meta_box($title ) {
    add_meta_box('image_sliderId', 'logos', 'add_image_slider', 'page', 'normal', 'default');
    
}

// Print custom meta box content
function add_image_slider() {
    global $post;
    wp_enqueue_media();

    $imageSliderData = get_post_meta($post->ID, 'image_data', true);
     // Use nonce for verification
    wp_nonce_field(plugin_basename(__FILE__), 'noncename_so_image'); 
    ?>
    <div id="dynamic_form">
        <div id="parent-row" >
            <?php
            if (isset($imageSliderData['image_url'])) {
                for ($i = 0; $i < count($imageSliderData['image_url']); $i++) {
            ?>
                <!--original field-->
                    <div class="field_row">
                        <div class="field_left">
                            <div class="form_field">
                                <input type="hidden" class="meta_image_url" 
                                    name="image[image_url][]" 
                                    value="<?php esc_html_e($imageSliderData['image_url'][$i]); ?>" />
                            </div>
                        </div>
 
                        <div class="field_right image_wrap">
                            <img src="<?php esc_html_e($imageSliderData['image_url'][$i]); ?>"
                                      height="130" width="128" />
                        </div>
                        <div class="field_right">
                            <input type="button" class="button btn-add" value="<?php _e('Edit','achisraeli'); ?>" 
                                       onclick="add_image(this)" title="<?php _e('Click to edit image','achisraeli'); ?>"/><br />
                            <input type="button" class="button btn-remove"  
                                      value="<?php _e('Remove','achisraeli'); ?>"    
                                      onclick="remove_field(this)" title="<?php _e('Click to remove image','achisraeli'); ?>"/>
                        </div>
                        <div class="clear" /></div> 
                    </div>
                <?php
            } // endif
        } // endforeach
        ?>
        </div>
        <!--empty jquery field-->
        <div style="display:none" id="child-row">
            <div class="field_row">
                <div class="field_left">
                    <div class="form_field">
                        <!--<span> No image selected</span>-->
                        <input type="hidden" class="meta_image_url" 
                                  name="image[image_url][]" value=""/>
                    </div>
                </div>
                <div class="field_right image_wrap">
                </div> 
                <div class="field_right"> 
                    <input type="button" class="button btn-add" 
                               value="<?php _e('Add','achisraeli'); ?>" onclick="add_image(this)" /><br />
                    <input type="button" class="button btn-remove" 
                               value="<?php _e('Remove','achisraeli'); ?>" onclick="remove_field(this)" /> 
                </div>
                <div class="clear"></div>
            </div>
        </div>
 
        <div id="add_field_row">
            <input class="button btn-addRow" type="button"
                     value="<?php _e('Add image','achisraeli'); ?>" onclick="add_field_row();" 
                     title="<?php _e('Click to add another image','achisraeli'); ?>"/>
        </div>
    </div>
    <?php
}
add_action('save_post', 'save_update_image_slider', 10, 2);
 
// Save post action, for Image slider
function save_update_image_slider($post_id, $post_object) {
	// Checks save status
    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    $is_valid_nonce = ( isset($_POST['prfx_nonce']) && wp_verify_nonce($_POST['prfx_nonce'], basename(__FILE__)) ) ? 'true' : 'false';
 
	// Exits script depending on save status
    if ($is_autosave || $is_revision || !$is_valid_nonce) {
        return;
    }
 
    if ($_POST['image']) {
        $imageSliderData = array(); // Build array for saving post meta
        for ($i = 0; $i < count($_POST['image']['image_url']); $i++) {
            if ('' != $_POST['image']['image_url'][$i]) {
                $imageSliderData['image_url'][] = $_POST['image']['image_url'][$i];
            }
        }
 
        if ($imageSliderData)
            update_post_meta($post_id, 'image_data', $imageSliderData);
        else
            delete_post_meta($post_id, 'image_data');
    }
    else {
        delete_post_meta($post_id, 'image_data');
    }

}
add_action('admin_enqueue_scripts', 'include_custom_js_css');
// Add custom JS and CSS
function include_custom_js_css() {
   wp_enqueue_script('image-repeater', get_theme_file_uri('/admin_assets/js/image-repeater.js'), array(), '1.0', true); //JS
   wp_enqueue_style('image-repeater', get_theme_file_uri('/admin_assets/css/image-repeater.css'), array(), '1.0'); //CSS
}