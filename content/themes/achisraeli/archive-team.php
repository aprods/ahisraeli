<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package achisraeli
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="">
			
					<?php if ( have_posts() ) : ?>
					<header class="entry-header">
						<div class="header-placeholder">
							<h1 class="entry-title"><?php _e('Team','achisraeli') ?></h1>
						</div>
						</header><!-- .entry-header -->

					<div class="row">
						<div class="col">
							<div class="grid" data-masonry='{ "itemSelector": ".grid-item","isRTL": true,"isOriginLeft": false}'>
								<?php
								/* Start the Loop */
								while ( have_posts() ) :
									the_post();

									?>
										  <div class="grid-item">
										  	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
										  		<?php achisraeli_post_thumbnail(); ?>
										  		<div class="mask">
											  		<h1 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title();?></a></h1>
											  	</div>

											</article><!-- #post-<?php the_ID(); ?> -->
										  </div>
										
									
									<?php

								endwhile;

								
								?>
							</div>
						</div>
						<!-- <div class="col-sm-4"><?php //get_sidebar(); ?></div> -->
					</div>

<?php

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>
			
		

		</main><!-- #main -->
	</div><!-- #primary -->
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

<?php
get_footer();
