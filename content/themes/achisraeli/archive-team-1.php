<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package achisraeli
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="">
			
					<?php if ( have_posts() ) : ?>
					<header class="entry-header">
						<div class="header-placeholder">
							<h1 class="entry-title"><?php _e('Team','achisraeli') ?></h1>
						</div>
						</header><!-- .entry-header -->

						<div class="row">

						<?php
						/* Start the Loop */
						while ( have_posts() ) :
							the_post();

							?>
							<div class="col-sm-3">
								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<header class="entry-header">
									<?php
									if ( is_singular() ) :
										the_title( '<h1 class="entry-title">', '</h1>' );
									else :
										the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
									endif;

									?>
								</header><!-- .entry-header -->

								<?php achisraeli_post_thumbnail(); ?>

								<div class="entry-content">
									<?php
									the_excerpt();
									?>
								</div><!-- .entry-content -->

								
							</article><!-- #post-<?php the_ID(); ?> -->
							</div>
							<?php

						endwhile;

						the_posts_navigation();
						?>
				<!-- <div class="col-sm-4"><?php //get_sidebar(); ?></div> -->
			</div>

<?php

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>
			
		

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
