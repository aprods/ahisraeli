[![Build Status](https://travis-ci.org/Automattic/_s.svg?branch=master)](https://travis-ci.org/Automattic/_s)

# achisraeli

A starter theme called achisraeli. Based oon Underscores https://underscores.me/.
Included SASS, gulp, uglify, etc...
With composer for quick install.

Enjoy!

## Getting Started

The first thing you want to do is copy the `achisraeli` directory and change the name to something else (like, say, `megatherium-is-awesome`), and then you'll need to do a five-step find and replace on the name in all the templates.

1. Search for: `'achisraeli'` and replace with: `'megatherium-is-awesome'`
2. Search for: `achisraeli_` and replace with: `megatherium_is_awesome_`
3. Search for: `Text Domain: achisraeli` and replace with: `Text Domain: megatherium-is-awesome` in `style.css`.
4. Search for: <code>&nbsp;achisraeli</code> and replace with: <code>&nbsp;Megatherium_is_Awesome</code>
5. Search for: `achisraeli-` and replace with: `megatherium-is-awesome-`

Then, update the stylesheet header in `style.css`, the links in `footer.php` with your own information and rename `achisraeli.pot` from `languages` folder to use the theme's slug. Next, update or delete this readme.

Now you're ready to go! The next step is easy to say, but harder to do: make an awesome WordPress theme. :)

Good luck!
