<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package achisraeli
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<div class="row align-items-center justify-content-between">
				
				<?php
					the_custom_logo();
					 echo do_shortcode('[social]') ?>
				
			</div>
			<div class="footer-row">
			<div class="copy">
					<?php echo get_theme_mod('copyright');?> | 
					<?php $tcPage = get_post(get_theme_mod('tc')); ?> 
					<a href="<?php echo get_permalink($tcPage) ;?>"><?php echo get_the_title($tcPage) ;?></a>
			</div>
			<div class="studio">
					<a href="https://pinesstudio.co.il " target="_blank"><img src="https://achisraeli.org/wp-content/uploads/2018/12/PinesStudio_Logo.png"></a>
			</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<?php echo get_theme_mod('headerCode'); ?>
</body>
</html>
