jQuery(document).ready(function( $ ) {
	checkSize();
	// Hide Header on on scroll down
	// var didScroll;
	// var lastScrollTop = 0;
	var delta = 50;
	var prevScrollpos = window.pageYOffset;
	var navbarHeight = $('.site-header').outerHeight();
	var didRun = false;
	$('body').css('margin-top',navbarHeight);

	

	$(window).scroll(function(event){
	    

	      var currentScrollpos = window.pageYOffset;
	      if(Math.abs(prevScrollpos - currentScrollpos) <= delta)
	        return;
	      if(prevScrollpos + delta > currentScrollpos) {
	            $(".site-header").css("top","0");
	      } else {
	            $(".site-header").css("top",navbarHeight * -1);
	      }

	      prevScrollpos = currentScrollpos;

	    
	    
	});
	$(window).resize(function(){     
       checkSize();

	});

	


	// press release page/ blog archive ....
    if ($('.post-filters').length) {
        // filters
        $(".post-filters a").on('click', function (event) {
            event.preventDefault();
        });
        $(".post-filters .filter").on('click', function () {
            $(this).addClass('active').siblings().removeClass('active');
            var filterName = $(this).data().filter || "";
            $(".post-grid").attr('data-filter', filterName);
            $(".post-grid .post-item").each(function () {
            	$(this).fadeOut();
                if ($(this).hasClass(filterName)) {
                    $(this).fadeIn();
                } else {
                    if (filterName == 'ALL') {
                        $(this).fadeIn();
                    } else {
                        $(this).fadeOut();
                    }
                }
            });
            // $(".post-grid article").each(function () {
            //     if ($(this).hasClass(filterName)) {
            //         $(this).show();
            //     } else {
            //         if (filterName == 'ALL') {
            //             $(this).show();
            //         } else {
            //             $(this).hide();
            //         }
            //     }
            // });
        });
    }

});
function checkSize(){
	    if (jQuery(".js-float").css("float") == "left" ){
	    	mobile=true;
	    }else{
	    	 mobile=false;
	    }
	}
	
function openTab(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
