<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package achisraeli
 */

get_header();
?>

		<div id="primary" class="content-area">
		<main id="main" class="">
			
					
					

		<?php
		while ( have_posts() ) :
			the_post();

			?>
			<header class="entry-header">
				<div class="header-placeholder">
					<?php achisraeli_post_thumbnail(); ?>
					<h1 class="entry-title"><?php the_title() ?></h1>
				</div>
			</header><!-- .entry-header -->

						<div class="row">
						<div class="col-sm-12">
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<div class="entry-content">
									<?php
									the_content();

									
									?>
								</div><!-- .entry-content -->

								<?php if ( get_edit_post_link() ) : ?>
									<footer class="entry-footer">
										<?php
										edit_post_link(
											sprintf(
												wp_kses(
													/* translators: %s: Name of current post. Only visible to screen readers */
													__( 'Edit <span class="screen-reader-text">%s</span>', 'achisraeli' ),
													array(
														'span' => array(
															'class' => array(),
														),
													)
												),
												get_the_title()
											),
											'<span class="edit-link">',
											'</span>'
										);
										?>
									</footer><!-- .entry-footer -->
								<?php endif; ?>
							</article><!-- #post-<?php the_ID(); ?> -->
						</div>

			<?php

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile;

						?>
							
				<!-- <div class="col-sm-4"><?php //get_sidebar(); ?></div> -->
			</div>
		

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
