<?php
/**
 * The template for displaying all pages
 *
 * Template Name: Landing page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package achisraeli
 */

get_header();

?>
		
		<?php
		while ( have_posts() ) :
			the_post();
			$f = get_fields($post->ID);
			// echo '<pre>';
			// 	print_r( $f);
			// echo '</pre>';
			?>



			<article id="post-<?php the_ID(); ?>" <?php post_class('no-sidebar'); ?>>
					<div id="primary" class="content-area">
			<main id="main" class="site-main">
			<header class="entry-header">
			<div class="header-placeholder">
				<?php achisraeli_post_thumbnail(); ?>
				<h1 class="entry-title"><?php _e('slogan','achisraeli');?> </h1>
				<?php //the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</div>
			</header><!-- .entry-header -->
			
					
						

					

					<div class="entry-content">

						<div class="container">
							<div class="row ">
								<div class="col-sm-7">
									<div class="row content-rectangle">
										<div class="col-sm-3">			
											<img src="<?php echo $f['about-icon']['url']; ?>" alt="<?php echo get_post_meta( $f['about-icon']['ID'], '_wp_attachment_image_alt', true) ?>" />
											<h2><?php echo $f['about-title'];?>
												<svg width="35" height="10" viewBox="0 0 35 10" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0.878717 0L0.372506 0.38145L0 2.8769L1.67849 3.0425L1.90594 7.20877L33.4148 9.99569L33.4651 10L34.1016 9.51948L33.7247 3.0425L0.927561 0.00430783L0.878717 0Z" />
												</svg>
											</h2>
										</div>
										<div class="col-sm-9">
											<?php echo $f['about-content'];?>
											<?php 
											if ($f['about-link']) {
												echo '<a href="'.$f['about-link'].'">'.__('read more','achisraeli').'</a>';
											}
											?>

										</div>
									</div>
									<a href="#home-form" class="button home-btn"><?php echo __('submission','achisraeli');?></a>
									<div class="content-rectangle">
										<!-- Tab links -->
										<div class="ntab">
											<button class='tablinks active' onclick="openTab(event, 'tab1')"><img src="<?php echo $f['plan-icon']['url']; ?>" alt="<?php echo get_post_meta( $f['plan-icon']['ID'], '_wp_attachment_image_alt', true) ?>" /><h2><?php echo $f['plan-title'];?><svg width="35" height="10" viewBox="0 0 35 10" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0.878717 0L0.372506 0.38145L0 2.8769L1.67849 3.0425L1.90594 7.20877L33.4148 9.99569L33.4651 10L34.1016 9.51948L33.7247 3.0425L0.927561 0.00430783L0.878717 0Z" />
												</svg></h2></button>
											<button class='tablinks' onclick="openTab(event, 'tab2')"><img src="<?php echo $f['target-icon']['url']; ?>" alt="<?php echo get_post_meta( $f['target-icon']['ID'], '_wp_attachment_image_alt', true) ?>" /><h2><?php echo $f['target-title'];?><svg width="35" height="10" viewBox="0 0 35 10" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0.878717 0L0.372506 0.38145L0 2.8769L1.67849 3.0425L1.90594 7.20877L33.4148 9.99569L33.4651 10L34.1016 9.51948L33.7247 3.0425L0.927561 0.00430783L0.878717 0Z" />
												</svg></h2></button>
											<button class='tablinks' onclick="openTab(event, 'tab3')"><img src="<?php echo $f['value-icon']['url']; ?>" alt="<?php echo get_post_meta( $f['value-icon']['ID'], '_wp_attachment_image_alt', true) ?>" /><h2><?php echo $f['value-title'];?><svg width="35" height="10" viewBox="0 0 35 10" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M0.878717 0L0.372506 0.38145L0 2.8769L1.67849 3.0425L1.90594 7.20877L33.4148 9.99569L33.4651 10L34.1016 9.51948L33.7247 3.0425L0.927561 0.00430783L0.878717 0Z" />
												</svg></h2></button>
										</div>
										<!-- Tab content -->
										
										<div id="tab1" class="tabcontent active" style="display: block;">
											<?php echo $f['plan-content'];?>
											<?php 
											if ($f['plan-link']) {
												echo '<a href="'.$f['plan-link'].'">'.__('read more','achisraeli').'</a>';
											}
											?>
											
										</div>
										
										<div id="tab2" class="tabcontent">
										 	<?php echo $f['target-content'];?>
										 	<?php 
											if ($f['target-link']) {
												echo '<a href="'.$f['target-link'].'">'.__('read more','achisraeli').'</a>';
											}
											?>
										</div>
										
										<div id="tab3" class="tabcontent">
										  <?php echo $f['value-content'];?>
										  <?php 
											if ($f['value-target']) {
												echo '<a href="'.$f['value-target'].'">'.__('read more','achisraeli').'</a>';
											}
											?>
										</div>
										
									</div>
								</div>
								<div class="col-sm-5">
									<div class="form-container" id="home-form">
										<h2 class="text-green"><?php echo $f['form-title'];?></h2>
										<p><?php echo $f['from-text'];?></p>
										<?php echo do_shortcode('[contact-form-7 id="'.$f['from-id'].'" title="Contact form 1"]') ?>
									</div>
									<div class="lp-map">
										<h2 class="text-green"><?php echo $f['inField-title'];?></h2>
										<!-- <img src="<?php //echo $f['inField-image']['url']; ?>" alt="<?php //echo get_post_meta( $f['inField-image']['ID'], '_wp_attachment_image_alt', true) ?>" /> -->
										<div>
											<?php
											 $places = $f['inField-place'];
											 $places = str_replace(';', '</span> <span><i class="fas fa-map-marker-alt text-green"></i> ', $places);
											 echo '<span><i class="fas fa-map-marker-alt text-green"></i> '.$places;
											?>
										</div>
									</div>

								</div>
							</div>
						</div>




						<?php
						// the_content();

						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'achisraeli' ),
							'after'  => '</div>',
						) );
						?>
					</div><!-- .entry-content -->

					<?php if ( get_edit_post_link() ) : ?>
						<footer class="entry-footer">
							<?php
							edit_post_link(
								sprintf(
									wp_kses(
										/* translators: %s: Name of current post. Only visible to screen readers */
										__( 'Edit <span class="screen-reader-text">%s</span>', 'achisraeli' ),
										array(
											'span' => array(
												'class' => array(),
											),
										)
									),
									get_the_title()
								),
								'<span class="edit-link">',
								'</span>'
							);
							?>
						</footer><!-- .entry-footer -->
					<?php endif; ?>
				
			<?php

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			?>
			</main><!-- #main -->
			</div><!-- #primary -->
			</article><!-- #post-<?php the_ID(); ?> -->
		<?php

		endwhile; // End of the loop.
		?>

		
	

<?php
// get_sidebar();
get_footer();
